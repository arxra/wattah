# Watering project

This is a small python Web-Server which expose some unauthenticated REST
endpoints which allows you to:

- Run a pump for a set amount of time
- Read moisture data from soil
- Read the presence of water.

The hardware this was tested with was a Raspberry Pi Zero, a MSP3002, a
[watering
unit](https://www.elfa.se/en/watering-unit-with-moisture-sensor-and-water-pump-m5stack-u101/p/30213351)
(featuring moisture sensor and water pump) and two pieces of aluminum
foil.

## Moisture sensing

The built in moisture sensing unit of the watering thingey-majigg sends
out the moisture of the soil as a analog 0-3.3 volt signal. This is
uninterpretable by a raspberry pi which cannot parse analog signals on
its own. However, it does have [hardware
SPI](https://pinout.xyz/pinout/spi#). So, we use a analog-to-digital
chip which reads a analog signal and outputs it over SPI. We then expose
this over REST, so the moisture can be read remotely.

Note: for some reason, it is reversed meaning the higher this value
goes, the dryer the soil. Not hard working around, but something to keep
in mind.

## Water sensing

Two options for sensing the presence of water:

- Buy a water sensor, read its documentation and implement it
- Assume water is conductive and assume water between 2 plates of
  aluminum foil acts as a closed circuit.

This project assumes the second approach. It’s really simple, with the
only drawback being one plate ionizing and the other de-ionizing over
time. 5 volts is more than enough to make this really apparent.

![setup](./img/IMG_20210517_120232.jpg)

## TODO:

- Make the polarization of the aluminium foil plates switch on regular
  intervals. Should be easy enough by connecting both ends to GPIO pins
  and swapping which pin is pulling up the voltage and reversing the
  read value. But me lazy.
- Authentication. Even just basic-auth would be great, as the security
  by obscurity it relies on now ain’t great. However, its fine since the
  WiFi it is connected to is (relatively) private and the REST endpoints
  are not exposed to the internet (unless configuration error).
