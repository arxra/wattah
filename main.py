import asyncio

from gpiozero import LED, MCP3002, Button
from aiohttp import web
import yaml

with open("conf.yml") as f:
    conf = yaml.safe_load(f)

pump = LED(conf["pump"])
water = Button(conf["water"], pull_up=False)
moisture = MCP3002(
    channel=conf["channel"],
    clock_pin=conf["clock_pin"],
    mosi_pin=conf["mosi_pin"],
    miso_pin=conf["miso_pin"],
    select_pin=conf["select_pin"],
)
routes = web.RouteTableDef()


@routes.get("/run_pump_dry")
async def pumpUntiDry(_):
    while water.is_active:
        pump.on()
        await asyncio.sleep(1)
    pump.off()
    return web.Response(body="Pump dry")


@routes.get("/run_pump")
async def hello(_):
    run_time = 5
    pump.on()
    await asyncio.sleep(run_time)
    pump.off()
    return web.Response(body="pump ran for {} seconds".format(run_time))


@routes.get("/status")
async def status(_):
    status = {}
    status["water"] = water.is_active
    return web.json_response(status)


@routes.get("/moisture")
async def moist(_):
    status = {}
    status["moisture"] = "%.3f" % moisture.value
    return web.json_response(status)


app = web.Application()
app.add_routes(routes)
web.run_app(app)
